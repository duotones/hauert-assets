"use strict";

function _possibleConstructorReturn(self, call) { if (call && (_typeof2(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _typeof2(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _typeof(t) {
  return (_typeof = "function" == typeof Symbol && "symbol" == _typeof2(Symbol.iterator) ? function (t) {
    return _typeof2(t);
  } : function (t) {
    return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : _typeof2(t);
  })(t);
}

function getJSON(t, e) {
  var i = new XMLHttpRequest();
  i.onreadystatechange = function () {
    if (4 == this.readyState && 200 == this.status) {
      var t = JSON.parse(this.responseText);
      e(t, this);
    }
  }, i.open("GET", t, !0), i.send();
}

var multilang = !1;
document.body.classList.contains("multilang") && (multilang = !0), document.addEventListener("DOMContentLoaded", function () {
  document.querySelector("html").classList.add("js");
}), window.NodeList && !NodeList.prototype.forEach && (NodeList.prototype.forEach = function (t, e) {
  e = e || window;

  for (var i = 0; i < this.length; i++) {
    t.call(e, this[i], i, this);
  }
}), !function () {
  if ("undefined" != typeof window) {
    var t = window.navigator.userAgent.match(/Edge\/(\d{2})\./),
        e = !!t && 16 <= parseInt(t[1], 10);

    if ("objectFit" in document.documentElement.style == 0 || e) {
      var i = function l(t, e, i) {
        var o, l, n, a, s;
        if ((i = i.split(" ")).length < 2 && (i[1] = i[0]), "x" === t) o = i[0], l = i[1], n = "left", a = "right", s = e.clientWidth;else {
          if ("y" !== t) return;
          o = i[1], l = i[0], n = "top", a = "bottom", s = e.clientHeight;
        }

        if (o !== n && l !== n) {
          if (o !== a && l !== a) return "center" === o || "50%" === o ? (e.style[n] = "50%", void (e.style["margin-" + n] = s / -2 + "px")) : void (0 <= o.indexOf("%") ? (o = parseInt(o)) < 50 ? (e.style[n] = o + "%", e.style["margin-" + n] = s * (o / -100) + "px") : (o = 100 - o, e.style[a] = o + "%", e.style["margin-" + a] = s * (o / -100) + "px") : e.style[n] = o);
          e.style[a] = "0";
        } else e.style[n] = "0";
      },
          o = function o(t) {
        var e = t.dataset ? t.dataset.objectFit : t.getAttribute("data-object-fit"),
            o = t.dataset ? t.dataset.objectPosition : t.getAttribute("data-object-position");
        e = e || "cover", o = o || "50% 50%";
        var n = t.parentNode;
        return function (t) {
          var e = window.getComputedStyle(t, null),
              i = e.getPropertyValue("position"),
              o = e.getPropertyValue("overflow"),
              n = e.getPropertyValue("display");
          i && "static" !== i || (t.style.position = "relative"), "hidden" !== o && (t.style.overflow = "hidden"), n && "inline" !== n || (t.style.display = "block"), 0 === t.clientHeight && (t.style.height = "100%"), -1 === t.className.indexOf("object-fit-polyfill") && (t.className = t.className + " object-fit-polyfill");
        }(n), function (t) {
          var e = window.getComputedStyle(t, null),
              i = {
            "max-width": "none",
            "max-height": "none",
            "min-width": "0px",
            "min-height": "0px",
            top: "auto",
            right: "auto",
            bottom: "auto",
            left: "auto",
            "margin-top": "0px",
            "margin-right": "0px",
            "margin-bottom": "0px",
            "margin-left": "0px"
          };

          for (var o in i) {
            e.getPropertyValue(o) !== i[o] && (t.style[o] = i[o]);
          }
        }(t), t.style.position = "absolute", t.style.width = "auto", t.style.height = "auto", "scale-down" === e && (e = t.clientWidth < n.clientWidth && t.clientHeight < n.clientHeight ? "none" : "contain"), "none" === e ? (i("x", t, o), void i("y", t, o)) : "fill" === e ? (t.style.width = "100%", t.style.height = "100%", i("x", t, o), void i("y", t, o)) : (t.style.height = "100%", void ("cover" === e && t.clientWidth > n.clientWidth || "contain" === e && t.clientWidth < n.clientWidth ? (t.style.top = "0", t.style.marginTop = "0", i("x", t, o)) : (t.style.width = "100%", t.style.height = "auto", t.style.left = "0", t.style.marginLeft = "0", i("y", t, o))));
      },
          n = function a(t) {
        if (void 0 === t || t instanceof Event) t = document.querySelectorAll("[data-object-fit]");else if (t && t.nodeName) t = [t];else {
          if ("object" != _typeof(t) || !t.length || !t[0].nodeName) return !1;
          t = t;
        }

        for (var a = 0; a < t.length; a++) {
          if (t[a].nodeName) {
            var i = t[a].nodeName.toLowerCase();
            "img" !== i || e ? "video" === i ? 0 < t[a].readyState ? o(t[a]) : t[a].addEventListener("loadedmetadata", function () {
              o(this);
            }) : o(t[a]) : t[a].complete ? o(t[a]) : t[a].addEventListener("load", function () {
              o(this);
            });
          }
        }

        return !0;
      };

      document.addEventListener("DOMContentLoaded", n), window.addEventListener("resize", n), window.objectFitPolyfill = n;
    } else window.objectFitPolyfill = function () {
      return !1;
    };
  }
}();

var ContentSelector =
/*#__PURE__*/
function () {
  function ContentSelector(selector) {
    _classCallCheck(this, ContentSelector);

    this.root = _typeof2(selector) === 'object' ? selector : document.querySelector(selector);
    this.contents = this.root.querySelectorAll('.selector-content');
    this.baseSelect = this.root.querySelector('.base-select');
    this.detailSelect = this.root.querySelector('.detail-select');
    this.current = '';
    this.selects = {};
    this.init();
  }

  _createClass(ContentSelector, [{
    key: "init",
    value: function init() {
      var options = [];
      var unique = {};
      this.contents.forEach(function (content) {
        var opt = {
          value: content.dataset.path.split(',')[0],
          name: content.dataset.name.split(',')[0]
        };

        if (!unique[opt.value]) {
          unique[opt.value] = true;
          options.push(opt);
        }
      });
      this.addSelect(this.baseSelect, 'base', options);
      this.update('base');
    }
  }, {
    key: "addSelect",
    value: function addSelect(target, node, options) {
      var _this = this;

      var selected = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';
      var ele = document.createElement('select');
      ele.classList.add('select');
      options.forEach(function (option) {
        var opt = document.createElement('option');
        opt.value = option.value;
        opt.innerHTML = option.name;
        ele.appendChild(opt);
      });
      ele.value = selected;
      ele.addEventListener('change', function () {
        _this.update(node);
      });
      ele.classList.add("select-".concat(node));
      target.appendChild(ele);
      this.selects[node] = ele;
    }
  }, {
    key: "getPath",
    value: function getPath(source) {
      var path = [];

      for (var node in this.selects) {
        var select = this.selects[node];
        if (select.value) path.push(select.value);
        if (source === node) break;
      }

      return path;
    }
  }, {
    key: "update",
    value: function update(source) {
      var _this2 = this;

      var path = this.getPath(source);
      var pathString = '';
      this.detailSelect.innerHTML = '';
      path.forEach(function (node, index) {
        var options = [];
        var unique = {};
        var selector = ".selector-content[data-path^=\"".concat(pathString).concat(node, "\"]");

        _this2.root.querySelectorAll(selector).forEach(function (content) {
          var opt = {
            value: content.dataset.path.split(',')[index + 1],
            name: content.dataset.name.split(',')[index + 1]
          };

          if (!unique[opt.value] && opt.value) {
            unique[opt.value] = true;
            options.push(opt);
          }
        });

        if (options.length > 0) {
          _this2.addSelect(_this2.detailSelect, node, options, path[index + 1] ? path[index + 1] : '');
        }

        pathString += "".concat(node, ",");
      });

      if (this.root.querySelectorAll(".selector-content[data-path=\"".concat(pathString.slice(0, -1), "\"]")).length > 0) {
        this.current = pathString.slice(0, -1);
      } else {
        this.current = '';
      }

      this.updateContent();
    }
  }, {
    key: "updateContent",
    value: function updateContent() {
      var _this3 = this;

      this.contents.forEach(function (content) {
        if (content.dataset.path !== _this3.current) {
          content.classList.add('hidden');
        } else {
          content.classList.remove('hidden');
        }
      });
    }
  }]);

  return ContentSelector;
}();

document.querySelectorAll('.content-selector').forEach(function (ele) {
  var cs = new ContentSelector(ele);
});
/* TODO: change API token */

var GeoFilter =
/*#__PURE__*/
function () {
  function GeoFilter(root, items) {
    _classCallCheck(this, GeoFilter);

    this.root = _typeof2(root) === 'object' ? root : document.querySelector(root);
    this.items = _typeof2(items) === 'object' ? items : document.querySelectorAll(items);
    this.input = this.root.querySelector('input');
    this.locator = this.root.querySelector('.locator');
    this.mapboxToken = 'pk.eyJ1IjoidGhvbWFzZ3V0aHJ1ZiIsImEiOiJjanBiYWM1dGsybGczM3Zsa2FzcTR1djZrIn0.Q84S3BWYlj8nh1m6cQg5bg';
    this.mapboxEndpoint = 'https://api.mapbox.com/geocoding/v5/mapbox.places/[query].json?[options]&access_token=[token]';
    this.geocodingOptions = 'language=de&country=CH';
    this.reverseGeocodingOptions = 'language=de&country=CH&types=place';
    this.mapUrl = this.root.dataset.mapUrl;
    this.map = {};
    this.place = null;
    this.region = null;
    this.loadMap();
    this.initEvents();
  }

  _createClass(GeoFilter, [{
    key: "loadMap",
    value: function loadMap() {
      var _this4 = this;

      getJSON(this.mapUrl, function (data) {
        _this4.map = data;
      });
    }
  }, {
    key: "initEvents",
    value: function initEvents() {
      var _this5 = this;

      if ('geolocation' in navigator) {
        this.locator.addEventListener('click', function () {
          navigator.geolocation.getCurrentPosition(function (position) {
            var coords = [position.coords.longitude, position.coords.latitude];
            getJSON(_this5.mapboxApi(coords.join(','), 'reverse'), function (data) {
              var place = data.features[0];

              if (place) {
                _this5.place = place.place_name;
              }

              _this5.region = _this5.getRegion(coords);

              _this5.update();
            });
          });
        });
      }

      this.root.addEventListener('submit', function (e) {
        e.preventDefault();
        var value = _this5.input.value;

        if (value) {
          getJSON(_this5.mapboxApi(value), function (data) {
            var place = data.features[0];

            if (place) {
              _this5.place = place.place_name;
              _this5.region = _this5.getRegion(data.features[0].center);

              _this5.update();
            }
          });
        } else {
          _this5.place = null;
          _this5.region = null;

          _this5.update();
        }
      });
      this.input.addEventListener('keydown', function (e) {
        if (_this5.region) {
          _this5.place = null;
          _this5.region = null;

          _this5.update();
        }
      });
    }
  }, {
    key: "update",
    value: function update() {
      var _this6 = this;

      if (this.place) {
        this.input.value = this.place;
      }

      if (this.region) {
        this.items.forEach(function (ele) {
          if (ele.dataset.region === _this6.region) {
            ele.style.display = 'block';
          } else {
            ele.style.display = 'none';
          }
        });
      } else {
        this.items.forEach(function (ele) {
          ele.style.display = 'block';
        });
      }
    }
  }, {
    key: "getRegion",
    value: function getRegion(coords) {
      var region = null;
      var point = turf.point(coords, {});
      this.map.features.forEach(function (feature) {
        var polygon = feature.geometry.geometries[0];

        if (turf.inside(point, polygon)) {
          region = feature.properties.name;
        }
      });
      return region;
    }
  }, {
    key: "mapboxApi",
    value: function mapboxApi(query, type) {
      var obj = {
        '[query]': query,
        '[options]': type === 'reverse' ? this.reverseGeocodingOptions : this.geocodingOptions,
        '[token]': this.mapboxToken
      };
      return this.mapboxEndpoint.replace(/\[query\]|\[options\]|\[token\]/gi, function (matched) {
        return obj[matched];
      });
    }
  }]);

  return GeoFilter;
}();

document.querySelectorAll('.geo-filter').forEach(function (element) {
  var items = document.querySelectorAll('.card[data-region]');
  var geoFilter = new GeoFilter(element, items);
});

var Modal =
/*#__PURE__*/
function () {
  function Modal() {
    _classCallCheck(this, Modal);

    this.created = false;
    this.opened = false;
    this.root = null;
    this.overlay = null;
    this.container = null;
    this.closeButton = null;
    this.type = 'text';
    this.localStorageId = null;
    this.player = null;
    this.preparePreview();
    this.updatePreview();
  }

  _createClass(Modal, [{
    key: "open",
    value: function open(options) {
      if (!this.created) {
        this.create();
        this.created = true;
      }

      if (!this.opened) {
        document.querySelector("body").append(this.root);
        this.opened = true;
        var content = '';

        if (options.title || options.content) {
          this.type = 'text';
          var text = options.title ? "<h3 class=\"title\">".concat(options.title, "</h3>") : '';
          text += options.content ? "<div>".concat(options.content, "</div>") : '';
          content += "<div class=\"text-container\">".concat(text, "</div>");
        } else if (options.ytid) {
          this.type = 'youtube';
          this.localStorageId = options.ytid;
          var url = "https://www.youtube.com/embed/".concat(options.ytid, "?enablejsapi=1&autoplay=1&modestbranding=1");
          content = "<div class=\"player-container\"><iframe src=\"".concat(url, "\" allowFullscreen></iframe></div>");
        } else if (options.videourl) {
          this.type = 'video';
          this.localStorageId = options.videourl;
          content += "<div class=\"player-container\"><video src=\"".concat(options.videourl, "\" controls autoplay></div>");
        }

        this.container.innerHTML = content;
        this.container.appendChild(this.closeButton);

        if (this.type === 'youtube') {
          this.initYoutube();
        } else if (this.type === 'video') {
          this.initVideo();
        }
      }
    }
  }, {
    key: "initYoutube",
    value: function initYoutube() {
      var _this7 = this;

      var start = 0;
      var data = localStorage.getItem(this.localStorageId);

      if (data) {
        var parsed = JSON.parse(data);
        start = Math.floor(parsed.progress * parsed.duration);
      }

      var video = this.container.querySelector('iframe');
      this.player = new YT.Player(video);
      this.player.addEventListener("onReady", function () {
        _this7.player.seekTo(start);
      });
      this.player.addEventListener("onStateChange", function (status) {
        if (status.data === 2) {
          _this7.setProgress();
        }
      });
    }
  }, {
    key: "initVideo",
    value: function initVideo() {
      var _this8 = this;

      var start = 0;
      var data = localStorage.getItem(this.localStorageId);

      if (data) {
        var parsed = JSON.parse(data);
        start = Math.floor(parsed.progress * parsed.duration);
      }

      var video = this.container.querySelector('video');
      this.player = video;
      this.player.currentTime = start;
      this.player.addEventListener("progress", function (status) {
        _this8.setProgress();
      });
    }
  }, {
    key: "setProgress",
    value: function setProgress() {
      var data = {};

      if (this.type === 'youtube') {
        var duration = this.player.getDuration();
        var progress = this.player.getCurrentTime() / duration;
        data = {
          progress: progress,
          duration: duration
        };
      } else if (this.type === 'video') {
        var _duration = this.player.duration;

        var _progress = this.player.currentTime / _duration;

        data = {
          progress: _progress,
          duration: _duration
        };
      }

      localStorage.setItem(this.localStorageId, JSON.stringify(data));
    }
  }, {
    key: "close",
    value: function close() {
      if (this.opened) {
        this.root.parentNode.removeChild(this.root);
        this.opened = false;

        if (this.type === 'youtube') {
          this.setProgress();
        } else if (this.type === 'video') {
          this.setProgress();
        }

        this.updatePreview();
      }
    }
  }, {
    key: "create",
    value: function create() {
      var root = document.createElement("div");
      root.classList.add('modal');
      this.root = root;
      var overlay = document.createElement("div");
      overlay.classList.add("modal-overlay");
      overlay.setAttribute("aria-hidden", "true");
      this.overlay = overlay;
      this.root.appendChild(overlay);
      var container = document.createElement("div");
      container.classList.add("modal-container");
      this.container = container;
      this.root.appendChild(container);
      var close = document.createElement("button");
      close.classList.add('modal-close');
      close.innerHTML = '<i class="material-icons">clear</i>';
      this.closeButton = close;
      this.container.appendChild(close);
      this.initEvents();
    }
  }, {
    key: "initEvents",
    value: function initEvents() {
      var _this9 = this;

      this.closeButton.addEventListener("click", function (e) {
        _this9.close();
      });
      this.overlay.addEventListener("click", function (e) {
        _this9.close();
      });
    }
  }, {
    key: "destroy",
    value: function destroy() {
      if (this.created) {
        this.root.parentNode.removeChild(this.root);
      }
    }
  }, {
    key: "preparePreview",
    value: function preparePreview() {
      var videos = document.querySelectorAll("[data-modal-yt], [data-modal-video]");
      videos.forEach(function (el) {
        var progress = document.createElement('div');
        progress.classList.add('modal-progress');
        progress.innerHTML = '<div class="status"></div>';
        el.appendChild(progress);
      });
    }
  }, {
    key: "updatePreview",
    value: function updatePreview() {
      var videos = document.querySelectorAll("[data-modal-yt], [data-modal-video]");
      videos.forEach(function (el) {
        var id = el.dataset.modalYt ? el.dataset.modalYt : el.dataset.modalVideo;
        var data = localStorage.getItem(id);

        if (data) {
          var parsed = JSON.parse(data);
          var progressbar = el.querySelector(".modal-progress");

          if (progressbar) {
            var status = progressbar.querySelector(".status");
            progressbar.classList.add("watched");
            status.style.transform = "scaleX(".concat(parsed.progress, ")");
          }
        }
      });
    }
  }]);

  return Modal;
}();

var modal = new Modal();
var youtubeModals = document.querySelectorAll("[data-modal-yt]");
var videoModals = document.querySelectorAll("[data-modal-video]");
var textModals = document.querySelectorAll('[data-modal]');
youtubeModals.forEach(function (el) {
  el.addEventListener("click", function () {
    modal.open({
      ytid: el.dataset.modalYt
    });
  });
});
videoModals.forEach(function (el) {
  el.addEventListener("click", function () {
    modal.open({
      videourl: el.dataset.modalVideo
    });
  });
});
textModals.forEach(function (el) {
  el.addEventListener("click", function () {
    var data = el.dataset.modal;

    if (data) {
      data = JSON.parse(data);
      modal.open({
        title: data.title,
        content: data.content
      });
    }
  });
});

var NavMultilevel =
/*#__PURE__*/
function () {
  _createClass(NavMultilevel, [{
    key: "level",
    get: function get() {
      return this.path.length;
    }
  }]);

  function NavMultilevel(selector) {
    _classCallCheck(this, NavMultilevel);

    this.root = _typeof2(selector) === 'object' ? selector : document.querySelector(selector);
    this.topLevel = this.root.querySelector('.top-level');
    this.levels = this.root.querySelectorAll('.nav-level');
    this.links = this.root.querySelectorAll('.sub-link');
    this.back = this.root.querySelector('.nav-back');
    this.scroller = this.root.querySelectorAll('.scroll-indicator');
    this.path = [];
    this.addEventListeners();
    this.update();
  }

  _createClass(NavMultilevel, [{
    key: "addEventListeners",
    value: function addEventListeners() {
      var _this10 = this;

      this.links.forEach(function (v) {
        v.addEventListener('click', function (e) {
          _this10.setPath(_this10.composedPath(v));

          _this10.update();
        });
      });
      this.back.addEventListener('click', function (e) {
        _this10.path.splice(-1, 1);

        _this10.update(true);
      });
      this.scroller.forEach(function (el) {
        el.addEventListener('click', function (e) {
          var dir = el.dataset.dir;

          if (_this10.root.scrollBy) {
            _this10.root.querySelector('.active').scrollBy({
              top: 200 * dir,
              left: 0,
              behavior: 'smooth'
            });
          } else {
            _this10.root.querySelector('.active').scrollTop += 200 * dir;
          }
        });
      });
      this.root.querySelectorAll('.nav-level').forEach(function (el) {
        el.addEventListener('scroll', function () {
          _this10.updateScroller();
        });
      });
    }
  }, {
    key: "goTo",
    value: function goTo(target, noAnimation) {
      var _this11 = this;

      var ele = this.root.querySelector(".nav-level[data-level=\"".concat(target, "\"]"));
      var parent = this.topLevel;

      if (ele) {
        parent = ele.parentNode;
      }

      var path = this.composedPath(parent);
      this.setPath(path);

      if (noAnimation) {
        this.root.classList.add('no-animation');
        window.setTimeout(function () {
          _this11.root.classList.remove('no-animation');
        }, 10);
      }

      this.update();
    }
  }, {
    key: "setPath",
    value: function setPath(path) {
      var levels = [];
      var subLevel = null;
      path.forEach(function (ele) {
        if (ele.className.indexOf('nav-level') !== -1) {
          levels.push(ele);
        }

        if (!subLevel && ele.tagName === 'LI') {
          subLevel = ele.querySelector('.nav-level');
        }
      });
      levels.reverse();
      if (subLevel) levels.push(subLevel);
      this.path = levels;
    }
  }, {
    key: "update",
    value: function update(delayed) {
      if (this.path.length === 0) {
        this.path.push(this.topLevel);
      }

      if (this.path.length > 1) {
        this.back.classList.add('active');
      } else {
        this.back.classList.remove('active');
      }

      var offsetX = -100 * (this.path.length - 1);
      this.topLevel.style.transform = "translateX(".concat(offsetX, "%)");

      if (delayed) {
        var obj = this;
        this.topLevel.addEventListener('transitionend', function _tl() {
          obj.updateClasses();
          obj.topLevel.removeEventListener('transitionend', _tl);
        });
      } else {
        this.updateClasses();
      }
    }
  }, {
    key: "updateClasses",
    value: function updateClasses() {
      this.levels.forEach(function (v) {
        v.classList.remove('active');
        v.classList.remove('visible');
      });
      this.path.forEach(function (v) {
        v.classList.add('visible');
      });
      this.path[this.path.length - 1].classList.add('active');
      this.path[this.path.length - 1].scrollTop = 0;
      this.updateScroller();
    }
  }, {
    key: "updateScroller",
    value: function updateScroller() {
      this.scroller.forEach(function (el) {
        el.classList.remove('visible');
      });
      var active = this.root.querySelector('.nav-level.active');

      if (active.clientHeight < active.scrollHeight) {
        this.scroller.forEach(function (el) {
          if (el.dataset.dir == -1 && active.scrollTop > 20) el.classList.add('visible');
          if (el.dataset.dir == 1 && active.scrollTop < active.scrollHeight - active.clientHeight - 20) el.classList.add('visible');
        });
      }
    }
  }, {
    key: "composedPath",
    value: function composedPath(el) {
      var path = [];

      while (el) {
        path.push(el);
        el = el.parentElement;
      }

      return path;
    }
  }]);

  return NavMultilevel;
}();

if (document.querySelectorAll('.nav-multilevel').length > 0) {
  var mainNav = new NavMultilevel('.nav-multilevel');
}

if (document.querySelectorAll('.site-header').length > 0 && document.querySelectorAll('.nav-side-drawer').length > 0) {
  document.addEventListener('scroll', function (e) {
    if (window.pageYOffset > 60) {
      document.querySelector('.site-header').classList.add('on-scroll');
      document.querySelector('.nav-side-drawer').classList.add('on-scroll');
    } else {
      document.querySelector('.site-header').classList.remove('on-scroll');
      document.querySelector('.nav-side-drawer').classList.remove('on-scroll');
    }
  });
}

(function () {
  var sizesForm = document.getElementById('product-sizes');
  var quantityForm = document.getElementById('product-quantity');
  var snipCartAdd = document.querySelector('.snipcart-add-item');
  var visualPrice = document.querySelector('.product-price .value span');
  var product = {
    size: null,
    weight: null,
    quant: null,
    price: null
  };

  function updateButton(size, weight, quant, price) {
    snipCartAdd.setAttribute('data-item-custom1-value', size);
    snipCartAdd.setAttribute('data-item-custom1-options', size);
    snipCartAdd.setAttribute('data-item-weight', weight);
    snipCartAdd.setAttribute('data-item-quantity', quant);
    snipCartAdd.setAttribute('data-item-price', price);
  }

  if (snipCartAdd) {
    if (sizesForm || quantityForm) {
      product.size = snipCartAdd.getAttribute('data-item-custom1-value');
      product.weight = snipCartAdd.getAttribute('data-item-weight');
      product.quant = snipCartAdd.getAttribute('data-item-quantity');
      product.price = snipCartAdd.getAttribute('data-item-price');
    }
  }

  if (sizesForm) {
    sizesForm.querySelectorAll('label').forEach(function (el) {
      el.addEventListener('click', function () {
        var sizeInput = this.htmlFor;
        product.size = document.getElementById(sizeInput).getAttribute('data-size');
        product.weight = document.getElementById(sizeInput).value;
        product.price = document.getElementById(sizeInput).getAttribute('data-item-price');
        visualPrice.innerHTML = product.price;

        if (snipCartAdd) {
          updateButton(product.size, product.weight, product.quant, product.price);
        }
      });
    });
  }

  if (quantityForm) {
    quantityForm.querySelectorAll('button').forEach(function (el) {
      var input = el.parentNode.querySelector('input[type=number]');
      el.addEventListener('click', function (evt) {
        evt.preventDefault();

        if (el.classList.contains('input-minus') && input.value > input.min) {
          input.value--;
        }

        if (el.classList.contains('input-plus')) {
          input.value++;
        }

        product.quant = input.value;

        if (snipCartAdd) {
          updateButton(product.size, product.weight, product.quant, product.price);
        }
      });
    });
    quantityForm.querySelector('input').addEventListener('blur', function () {
      product.quant = this.parentNode.querySelector('input[type=number]').value;

      if (snipCartAdd) {
        updateButton(product.size, product.weight, product.quant, product.price);
      }
    });
  }
})();

var Tooltip =
/*#__PURE__*/
function () {
  function Tooltip() {
    _classCallCheck(this, Tooltip);

    this.root = document.querySelector('body');
    this.elements = document.querySelectorAll('[data-tooltip]');
    this.tooltip = null;
    this.content = '';
    this.classes = ['left', 'top'];
    this.pos = [0, 0];
    this.offset = 10;
    this.created = false;
    this.opened = false;
    this.create();
    this.initEvents();
  }

  _createClass(Tooltip, [{
    key: "initEvents",
    value: function initEvents() {
      var _this12 = this;

      this.elements.forEach(function (el) {
        el.addEventListener('mouseenter', function (e) {
          _this12.content = el.dataset.tooltip;
          var rect = el.getBoundingClientRect();
          _this12.classes = _this12.placing(rect);
          var left = rect.left;
          var top = rect.top;

          if (_this12.classes[0] === 'left') {
            left = rect.right + _this12.offset;
            top = rect.top + rect.height / 2;
          } else if (_this12.classes[0] === 'right') {
            left = rect.left - _this12.offset;
            top = rect.top + rect.height / 2;
          } else {
            left = rect.left + rect.width / 2;

            if (_this12.classes[1] === 'top') {
              top = rect.bottom + _this12.offset;
            } else {
              top = rect.top - _this12.offset;
            }
          }

          _this12.pos = [left, top];

          _this12.open();
        });
        el.addEventListener('mouseleave', function (e) {
          if (_this12.opened) _this12.close();
        });
      });
      this.tooltip.addEventListener('transitionend', function (e) {
        if (!_this12.opened) _this12.root.removeChild(_this12.tooltip);
      });
    }
  }, {
    key: "open",
    value: function open() {
      var _this13 = this;

      this.tooltip.innerHTML = this.content;
      this.tooltip.style.left = "".concat(this.pos[0], "px");
      this.tooltip.style.top = "".concat(this.pos[1], "px");
      this.tooltip.classList.remove('left');
      this.tooltip.classList.remove('center');
      this.tooltip.classList.remove('right');
      this.tooltip.classList.remove('top');
      this.tooltip.classList.remove('middle');
      this.tooltip.classList.remove('bottom');
      this.tooltip.classList.add(this.classes[0]);
      this.tooltip.classList.add(this.classes[1]);
      this.root.appendChild(this.tooltip);
      this.opened = true;
      setTimeout(function () {
        _this13.tooltip.classList.add('visible');
      }, 1);
    }
  }, {
    key: "close",
    value: function close() {
      this.opened = false;
      this.tooltip.classList.remove('visible');
    }
  }, {
    key: "create",
    value: function create() {
      var tt = document.createElement('div');
      tt.classList.add('tooltip');
      this.tooltip = tt;
    }
  }, {
    key: "placing",
    value: function placing(rect) {
      var left = rect.left + rect.width / 2;
      var right = window.innerWidth - left >= 0 ? window.innerWidth - left : 0;
      var top = rect.top;
      var bottom = window.innerHeight - rect.bottom >= 0 ? window.innerHeight - rect.bottom : 0;
      var mRatio = 6;
      var hRatio = left / right;
      var hPlacing = 'center';
      var vPlacing = 'top';

      if (hRatio < 1 / mRatio) {
        hPlacing = 'left';
      } else if (hRatio > mRatio) {
        hPlacing = 'right';
      }

      if (top > bottom) {
        vPlacing = 'bottom';
      }

      return [hPlacing, vPlacing];
    }
  }]);

  return Tooltip;
}();

var tooltip = new Tooltip();

var TypeAhead =
/*#__PURE__*/
function () {
  function TypeAhead(selector) {
    var _this14 = this;

    _classCallCheck(this, TypeAhead);

    this.query = '';
    this.selected = false;
    this.lastSelected = this.selected;
    this.selectIndex = 0;
    this.root = _typeof2(selector) === 'object' ? selector : document.querySelector(selector);
    this.input = this.root.querySelector('input');
    this.dropdown = this.root.querySelector('.options');
    this.options = [];
    this.results = [];
    this.root.querySelectorAll('.options .option').forEach(function (opt) {
      _this14.options.push({
        ele: opt,
        value: opt.dataset.value,
        name: opt.innerText
      });
    });
    this.numToShow = 20;
    this.open = false;
    this.noMouseEnter = false;
    this.initControl();
    this.setResults();
    this.update();
  }

  _createClass(TypeAhead, [{
    key: "initControl",
    value: function initControl() {
      var _this15 = this;

      this.input.addEventListener('keyup', function (e) {
        if (e.which === 13) {
          _this15.selected = _this15.results[_this15.selectIndex];

          _this15.input.blur();
        } else if (e.which === 27) {
          _this15.input.blur();
        } else if (e.which === 38) {
          _this15.selectIndex = _this15.selectIndex - 1 < 0 ? 0 : _this15.selectIndex - 1;

          _this15.scrollParentToChild();

          _this15.update();
        } else if (e.which === 40) {
          var max = _this15.results.length - 1;
          _this15.selectIndex = _this15.selectIndex + 1 > max ? max : _this15.selectIndex + 1;

          _this15.scrollParentToChild();

          _this15.update();
        } else {
          _this15.query = _this15.input.value;
          _this15.selectIndex = 0;
          _this15.dropdown.scrollTop = 0;
          _this15.numToShow = 20;

          _this15.setResults();

          _this15.update();
        }
      });
      this.options.forEach(function (opt) {
        opt.ele.addEventListener('mousedown', function () {
          _this15.selected = opt;
          _this15.selectIndex = parseInt(opt.ele.style.order);
        });
        opt.ele.addEventListener('mouseenter', function () {
          if (!_this15.noMouseEnter) {
            _this15.selectIndex = parseInt(opt.ele.style.order);

            _this15.update();
          }
        });
      });
      this.input.addEventListener('focus', function () {
        _this15.open = true;
        _this15.numToShow = 20; //this.query = ''
        //this.input.value = ''

        _this15.update();
      });
      this.input.addEventListener('blur', function () {
        _this15.open = false;
        _this15.dropdown.scrollTop = 0;

        _this15.update();
      });
      this.dropdown.addEventListener('scroll', function () {
        if (_this15.dropdown.scrollTop + 50 > _this15.dropdown.scrollHeight - _this15.dropdown.clientHeight && _this15.numToShow < _this15.options.length) {
          _this15.numToShow += 20;

          _this15.update();
        }
      });
    }
  }, {
    key: "setResults",
    value: function setResults() {
      var _this16 = this;

      var results = this.options.filter(function (opt) {
        return _this16.lookup(_this16.query, opt.name);
      });
      results = results.sort(function (a, b) {
        return a.name.localeCompare(b.name);
      });
      this.results = results;
    }
  }, {
    key: "update",
    value: function update() {
      var _this17 = this;

      var results = this.results.slice(0, this.numToShow);

      if (this.open) {
        this.root.classList.add('expanded');
      } else {
        this.root.classList.remove('expanded');
      }

      if (this.open) {
        this.options.forEach(function (opt) {
          opt.ele.classList.add('hidden');
        });
        results.forEach(function (res, index) {
          res.ele.classList.remove('hidden');
          res.ele.classList.remove('selected');
          res.ele.style.order = index;
          if (index === _this17.selectIndex) res.ele.classList.add('selected');
        });
      } else {
        this.input.value = this.selected ? this.selected.name : '';
      }

      if (this.selected !== this.lastSelected) {
        console.log(this.selected ? this.selected.value : false);
      }

      this.lastSelected = this.selected;
    }
  }, {
    key: "lookup",
    value: function lookup(needle, haystack) {
      var hlen = haystack.length;
      var nlen = needle.length;

      if (nlen > hlen) {
        return false;
      }

      needle = this.accentFold(needle.toLowerCase());
      haystack = this.accentFold(haystack.toLowerCase());

      if (nlen === hlen) {
        return needle === haystack;
      }

      outer: for (var i = 0, j = 0; i < nlen; i++) {
        var nch = needle.charCodeAt(i);

        while (j < hlen) {
          if (haystack.charCodeAt(j++) === nch) {
            continue outer;
          }
        }

        return false;
      }

      return true;
    }
  }, {
    key: "accentFold",
    value: function accentFold(inStr) {
      return inStr.replace(/([àáâãäå])|([ç])|([èéêë])|([ìíîï])|([ñ])|([òóôõöø])|([ß])|([ùúûü])|([ÿ])|([æ])/g, function (str, a, c, e, i, n, o, s, u, y, ae) {
        if (a) return 'a';else if (c) return 'c';else if (e) return 'e';else if (i) return 'i';else if (n) return 'n';else if (o) return 'o';else if (s) return 's';else if (u) return 'u';else if (y) return 'y';else if (ae) return 'ae';
      });
    }
  }, {
    key: "scrollParentToChild",
    value: function scrollParentToChild() {
      var _this18 = this;

      var parent = this.dropdown;
      var child = this.results[this.selectIndex].ele;
      var parentRect = parent.getBoundingClientRect();
      var parentViewableArea = {
        height: parent.clientHeight,
        width: parent.clientWidth
      };
      var childRect = child.getBoundingClientRect();
      var isViewable = childRect.top >= parentRect.top && childRect.top <= parentRect.top + parentViewableArea.height;

      if (!isViewable) {
        this.noMouseEnter = true;
        parent.scrollTop = childRect.top + parent.scrollTop - parentRect.top;
        window.setTimeout(function () {
          _this18.noMouseEnter = false;
        }, 100);
      }
    }
  }]);

  return TypeAhead;
}();

document.querySelectorAll('.typeahead').forEach(function (ele) {
  var ta = new TypeAhead(ele);
});

var Carousel =
/*#__PURE__*/
function () {
  _createClass(Carousel, [{
    key: "rootWidth",
    get: function get() {
      return this.root.offsetWidth;
    }
  }, {
    key: "itemWidth",
    get: function get() {
      return this.items[0].offsetWidth;
    }
  }, {
    key: "visibleAtOnce",
    get: function get() {
      return Math.round(this.rootWidth / this.itemWidth);
    }
  }]);

  function Carousel(root) {
    _classCallCheck(this, Carousel);

    this.root = _typeof2(root) === 'object' ? root : document.querySelector(root);
    this.opts = {
      itemSelector: this.root.dataset.itemSelector || '.item',
      containerSelector: this.root.dataset.containerSelector || '.item-container',
      navigationSelector: this.root.dataset.navigationSelector || '.item-navigation',
      touchControl: this.root.dataset.touchControl || true,
      keyControl: this.root.dataset.keyControl || false,
      nextSelector: this.root.dataset.nextSelector || '.item-next',
      prevSelector: this.root.dataset.prevSelector || '.item-prev',
      autoPlay: this.root.dataset.autoPlay || false
    };
    this.index = 0;
    this.container = this.root.querySelector(this.opts.containerSelector);
    this.items = this.root.querySelectorAll(this.opts.itemSelector);
    this.navigation = this.root.querySelector(this.opts.navigationSelector);
    this.prevButtons = this.root.querySelectorAll(this.opts.prevSelector);
    this.nextButtons = this.root.querySelectorAll(this.opts.nextSelector);
    var active = this.container.querySelector("".concat(this.opts.itemSelector, ".active"));

    if (active) {
      var itemIndex = Array.prototype.slice.call(this.items).indexOf(active);
      this.index = itemIndex - (this.visibleAtOnce - 1);

      if (this.index < 0) {
        this.index = 0;
      }
    }

    this.initControl();
    this.update(false);
  }

  _createClass(Carousel, [{
    key: "initControl",
    value: function initControl() {
      var _this19 = this;

      window.addEventListener('resize', function () {
        _this19.update(false);
      });

      if (this.navigation) {
        var links = this.navigation.querySelectorAll('li');
        links.forEach(function (v, i) {
          v.addEventListener('click', function (li) {
            _this19.index = i;

            _this19.update();
          });
        });
      }

      this.prevButtons.forEach(function (el) {
        el.addEventListener('click', function () {
          _this19.prev();
        });
      });
      this.nextButtons.forEach(function (el) {
        el.addEventListener('click', function () {
          _this19.next();
        });
      });

      if (this.opts.keyControl) {
        window.addEventListener('keydown', function (e) {
          if (e.which === 37) {
            _this19.prev();
          } else if (e.which === 39) {
            _this19.next();
          }
        });
      }

      if (this.opts.autoPlay) {
        var playing = true;
        var time = 0;
        this.root.addEventListener('mouseenter', function () {
          playing = false;
          time = 0;
        });
        this.root.addEventListener('mouseleave', function () {
          playing = true;
        });
        window.setInterval(function () {
          if (playing) {
            time++;

            if (time >= _this19.opts.autoPlay) {
              var maxIndex = _this19.items.length - _this19.visibleAtOnce;
              time = 0;
              _this19.index++;

              if (_this19.index > maxIndex) {
                _this19.index = 0;
              }

              _this19.update();
            }
          }
        }, 1000);
      }

      if (this.opts.touchControl) {
        var touchTrigger = false;
        var touchMove = false;
        var startPos = {
          x: 0,
          y: 0
        };
        this.root.addEventListener('touchstart', function (e) {
          if (e.touches.length === 1) {
            touchTrigger = true;
            startPos.x = e.touches[0].screenX;
            startPos.y = e.touches[0].screenY;
          }
        });
        document.addEventListener('touchmove', function (e) {
          if (touchTrigger) {
            touchMove = true;
            var pos = {
              x: e.touches[0].screenX,
              y: e.touches[0].screenY
            };

            _this19.noAnimation();

            var left = _this19.index * _this19.itemWidth * -1;
            _this19.container.style.transform = 'translate3D(' + (left + (pos.x - startPos.x)) + 'px, 0, 0)';
          }
        });
        document.addEventListener('touchend', function (e) {
          if (touchMove) {
            e.preventDefault();
            touchTrigger = false;
            touchMove = false;
            var pos = {
              x: e.changedTouches[0].screenX,
              y: e.changedTouches[0].screenY
            };
            var moved = (pos.x - startPos.x) / _this19.itemWidth;

            if (moved > 0.3) {
              _this19.prev();
            } else if (moved < -0.3) {
              _this19.next();
            } else {
              _this19.update();
            }
          }
        });
      }
    }
  }, {
    key: "next",
    value: function next() {
      this.index++;
      var maxIndex = this.items.length - this.visibleAtOnce;

      if (this.index > maxIndex) {
        this.index = maxIndex;
      }

      this.update();
    }
  }, {
    key: "prev",
    value: function prev() {
      this.index--;

      if (this.index < 0) {
        this.index = 0;
      }

      this.update();
    }
  }, {
    key: "update",
    value: function update() {
      var _this20 = this;

      var animate = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

      if (!animate) {
        this.noAnimation();
      } else {
        this.container.classList.remove('no-transition');
      }

      var left = this.index * this.itemWidth * -1;
      this.container.style.transform = 'translate3D(' + left + 'px, 0, 0)';

      if (this.navigation) {
        var links = this.navigation.querySelectorAll('li');
        links.forEach(function (v, i) {
          if (_this20.index === i) {
            v.classList.add('active');
          } else {
            v.classList.remove('active');
          }
        });
      }

      this.prevButtons.forEach(function (el) {
        if (_this20.index === 0) {
          el.classList.add('disabled');
        } else {
          el.classList.remove('disabled');
        }
      });
      this.nextButtons.forEach(function (el) {
        if (_this20.index === _this20.items.length - _this20.visibleAtOnce) {
          el.classList.add('disabled');
        } else {
          el.classList.remove('disabled');
        }
      });
    }
  }, {
    key: "noAnimation",
    value: function noAnimation() {
      var _this21 = this;

      this.container.classList.add('no-transition');
      setTimeout(function () {
        _this21.container.classList.remove('no-transition');
      }, 1);
    }
  }]);

  return Carousel;
}();

document.querySelectorAll('.carousel').forEach(function (element) {
  var carousel = new Carousel(element);
});

var Filter =
/*#__PURE__*/
function () {
  _createClass(Filter, [{
    key: "data",
    get: function get() {
      var data = [];
      this.sections.forEach(function (section) {
        var cat = section.dataset.section;
        var dataObj = {
          ele: section,
          name: cat,
          values: []
        };
        var checkboxes = section.querySelectorAll('.section-selection input[type="checkbox"]');
        checkboxes.forEach(function (checkbox) {
          if (checkbox.checked) {
            dataObj.values.push({
              name: checkbox.dataset.label,
              value: checkbox.value
            });
          }
        });
        data.push(dataObj);
      });
      return data;
    }
  }, {
    key: "json",
    get: function get() {
      var response = {};
      this.data.forEach(function (section) {
        if (section.values.length > 0) {
          response[section.name] = section.values.map(function (value) {
            return value.value;
          });
        }
      });
      if (this.query) response.query = this.query;
      if (this.sort) response.orderBy = this.sort.value;
      return JSON.stringify(response);
    }
  }]);

  function Filter(selector) {
    _classCallCheck(this, Filter);

    this.root = _typeof2(selector) === 'object' ? selector : document.querySelector(selector);
    this.sections = this.root.querySelectorAll('.filter-section');
    this.favorites = this.root.querySelectorAll('.favorite');
    this.input = this.root.querySelector('.search-bar');
    this.eraser = this.root.querySelector('.eraser');
    this.toggler = this.root.querySelectorAll('.toggle-filter');
    this.indicator = this.root.querySelector('.filter-indicator');
    this.sort = this.root.querySelector('.sort-section select');
    this.query = '';
    this.more = this.root.dataset.more ? this.root.dataset.more : 'more';
    this.ajaxuri = this.root.dataset.ajaxuri;
    this.lang = this.root.dataset.lang;
    this.profi = this.root.dataset.profi;
    this.datapid = this.root.dataset.datapid;
    this.itemSelector;
    this.itemType;
    this.initControl();
    this.update(true);
  }

  _createClass(Filter, [{
    key: "initControl",
    value: function initControl() {
      var _this22 = this;

      this.sections.forEach(function (v) {
        var header = v.querySelector('.section-header');
        header.addEventListener('click', function () {
          _this22.expandSection(v);
        });
      });
      this.root.querySelectorAll('input[type="checkbox"]').forEach(function (v) {
        v.addEventListener('change', function () {
          _this22.resetSearch();

          _this22.update();
        });
      });

      if ($) {
        if ($('#keepFilter')) {
          this.itemSelector = $('#keepFilter').data('itemselector');
          this.itemType = $('#keepFilter').data('itemtype');
        }

        var usage = [];
        var season = [];
        var query = '';
        var filters = localStorage.getItem(this.itemType + 'filters');
        var filtersSet = JSON.parse(filters);

        if (filtersSet) {
          if (filtersSet.hasOwnProperty('usage')) {
            usage = filtersSet.usage;
          }

          if (filtersSet.hasOwnProperty('season')) {
            season = filtersSet.season;
          }

          if (filtersSet.hasOwnProperty('query')) {
            query = filtersSet.query;
          }
        }

        if (usage.length) {
          $(usage).each(function (idx) {
            $('#usage-' + usage[idx]).prop("checked", true);
          });
        }

        if (season.length) {
          $(season).each(function (idx) {
            $('#season-' + season[idx]).prop("checked", true);
          });
        }

        if (query) {
          $('#productftsearch').val(query);
          this.query = query;
        }
      }

      this.input.addEventListener('submit', function (e) {
        e.preventDefault();
        _this22.query = _this22.input.querySelector('input').value;

        _this22.resetFilter();

        _this22.update();
      });
      this.eraser.addEventListener('click', function () {
        _this22.eraser.classList.remove('visible');

        _this22.resetSearch();

        _this22.update();
      });
      this.input.querySelector('input').addEventListener('keyup', function () {
        if (_this22.input.querySelector('input').value === '') {
          _this22.eraser.classList.remove('visible');
        } else {
          _this22.eraser.classList.add('visible');
        }
      });

      if (this.sort) {
        this.sort.addEventListener('change', function (e) {
          _this22.update();
        });
      }

      this.toggler.forEach(function (toggle) {
        toggle.addEventListener('click', function () {
          _this22.root.classList.toggle('is-expanded');

          if (_this22.root.classList.contains('is-expanded')) {
            document.querySelector('html').classList.add('no-scroll');
          } else {
            document.querySelector('html').classList.remove('no-scroll');
          }
        });
      });
    }
  }, {
    key: "expandSection",
    value: function expandSection(section) {
      var selection = section.querySelector('.section-selection');
      section.classList.toggle('is-expanded');
      this.root.querySelectorAll('.filter-section').forEach(function (v) {
        if (v.dataset.section != section.dataset.section) {
          v.classList.remove('is-expanded');
          v.querySelector('.section-selection').style.height = '';
        }
      });

      if (section.classList.contains('is-expanded')) {
        var height = selection.offsetHeight;
        selection.style.height = '0px';
        setTimeout(function () {
          selection.style.height = height + 'px';
        }, 1);
      } else {
        selection.style.height = '';
      }
    }
  }, {
    key: "resetSearch",
    value: function resetSearch() {
      this.input.querySelector('input').value = '';
      this.eraser.classList.remove('visible');
      this.query = '';
    }
  }, {
    key: "resetFilter",
    value: function resetFilter() {
      var checkboxes = this.root.querySelectorAll('input[type="checkbox"]');
      checkboxes.forEach(function (checkbox) {
        checkbox.checked = false;
      });
    }
  }, {
    key: "update",
    value: function update() {
      var _this23 = this;

      var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var activeFilters = 0;
      this.root.querySelectorAll('.section-selection input[type="checkbox"]').forEach(function (cb) {
        var id = cb.id;

        var fav = _this23.root.querySelector(".filter-favorites .favorite[for=\"".concat(id, "\"]"));

        if (cb.checked && fav) {
          fav.classList.remove('outline');
        } else if (!cb.checked && fav) {
          fav.classList.add('outline');
        } else if (cb.checked && !fav) {
          activeFilters++;
        }
      });

      if (activeFilters === 0) {
        this.indicator.classList.add('outline');
      } else {
        this.indicator.classList.remove('outline');
      }

      this.data.forEach(function (cat) {
        var chipsContainer = cat.ele.querySelector('.section-selected');
        chipsContainer.innerHTML = '';
        cat.values.forEach(function (val) {
          _this23.addChip("".concat(cat.name, "-").concat(val.value), val.name, chipsContainer);
        });

        if (cat.values.length > 0) {
          cat.ele.classList.add('has-selected');
        } else {
          cat.ele.classList.remove('has-selected');
        }
      });

      if (!init && $) {
        localStorage.setItem(this.itemType + 'filters', this.json);
        console.log(this.itemType + 'filters');
      }

      this.load();
      this.updateInfo();
    }
  }, {
    key: "load",
    value: function load() {
      var _this24 = this;

      if ($) {
        var usage = [];
        var season = [];
        var query = '';
        var filtersSet = JSON.parse(this.json);

        if (filtersSet.hasOwnProperty('usage')) {
          usage = filtersSet.usage;
        }

        if (filtersSet.hasOwnProperty('season')) {
          season = filtersSet.season;
        }

        if (filtersSet.hasOwnProperty('query')) {
          query = filtersSet.query;
        }

        $('#noresults').hide();

        if ($.trim(query)) {
          var ajaxuri;
          var getstr = '';

          if (this.itemType == 'product') {
            getstr = '&tx_hauertproducts_ajax[selector]=' + this.itemSelector + '&tx_hauertproducts_ajax[productspid]=' + this.datapid + '&tx_hauertproducts_ajax[lang]=' + this.lang + '&tx_hauertproducts_ajax[profi]=' + this.profi + '&tx_hauertproducts_ajax[query]=' + encodeURIComponent($.trim(query));
          }

          if (this.itemType == 'guide') {
            getstr = '&tx_hauertratgeber_ajax[selector]=' + this.itemSelector + '&tx_hauertratgeber_ajax[newspid]=' + this.datapid + '&tx_hauertratgeber_ajax[lang]=' + '&tx_hauertratgeber_ajax[query]=' + encodeURIComponent($.trim(query));
          }

          $.ajax({
            async: 'true',
            url: this.ajaxuri,
            dataType: 'json',
            type: 'POST',
            data: getstr
          }).done(function (data) {
            var itemSelector = data.selector;
            $(itemSelector).each(function (index) {
              if ($.inArray($(_this24).data("mwid"), data.results) > -1) {
                $(_this24).removeClass('hidden').fadeIn('slow');
              } else {
                $(_this24).addClass('hidden').fadeOut('slow');
              }
            });

            if (!data.results.length) {
              $('#noresults').show();
            }
          });
        } else {
          if (usage.length || season.length) {
            $(this.itemSelector).each(function (index) {
              var prodseasons = String($(_this24).data("season")).split("-");
              var prodtasks = String($(_this24).data("usage")).split("-");
              var show = false;

              if (usage.length) {
                $(usage).each(function (val) {
                  if ($.inArray(usage[val], prodtasks) > -1) {
                    show = true;
                  }
                });
              } else {
                show = true;
              }

              if (show) {
                if (season.length) {
                  var showseason = false;
                  $(season).each(function (val) {
                    if ($.inArray(season[val], prodseasons) > -1) {
                      showseason = true;
                    }
                  });

                  if (!showseason) {
                    show = false;
                  }
                }
              }

              if (show) {
                $(_this24).removeClass('hidden').fadeIn('slow');
              } else {
                $(_this24).addClass('hidden').fadeOut('slow');
              }
            });
          } else {
            $(this.itemSelector).each(function (index) {
              $(_this24).removeClass('hidden').fadeIn('slow');
            });
          }
        }

        this.hideEmpty();
      } else {
        console.log(this.json);
      }
    }
  }, {
    key: "updateInfo",
    value: function updateInfo() {
      var _this25 = this;

      this.sections.forEach(function (section) {
        var options = [];
        section.querySelectorAll('input[type="checkbox"]:not([disabled])').forEach(function (input) {
          if (!input.parentNode.parentNode.classList.contains('hidden') || !$) {
            options.push(input.dataset.label);
          }
        });
        var amount = options.length;
        var info = '';

        if (amount > 3) {
          info = "".concat(options.slice(0, 2).join(', '), ", +").concat(amount - 2, " ").concat(_this25.more);
        } else {
          info = options.join(', ');
        }

        section.querySelector('.description').innerHTML = info;
      });
    }
  }, {
    key: "addChip",
    value: function addChip() {
      var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var name = arguments.length > 1 ? arguments[1] : undefined;
      var target = arguments.length > 2 ? arguments[2] : undefined;
      var close = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
      var chip = document.createElement('label');
      chip.classList.add('chip');
      if (id) chip.setAttribute('for', id);
      chip.innerHTML = name;
      if (close) chip.innerHTML += '<span class="icon"><i class="material-icons">close</i></span>';
      target.appendChild(chip);
    }
  }, {
    key: "hideEmpty",
    value: function hideEmpty() {
      var _this26 = this;

      this.sections.forEach(function (section) {
        var atLeastOneChecked = false;
        section.querySelectorAll('input[type="checkbox"]:not([disabled])').forEach(function (input) {
          if (input.checked) {
            atLeastOneChecked = true;
          }
        });

        if (atLeastOneChecked) {
          section.querySelectorAll('input[type="checkbox"]:not([disabled])').forEach(function (input) {
            input.parentNode.parentNode.classList.remove('hidden');
          });
        } else {
          section.querySelectorAll('input[type="checkbox"]:not([disabled])').forEach(function (input) {
            if (!input.checked) {
              var hide = true;
              $(_this26.itemSelector).each(function (index) {
                if ($(_this26).data(section.dataset.section) !== 'undefined') {
                  var filtervals = String($(_this26).data(section.dataset.section)).split("-");

                  if (!$(_this26).hasClass("hidden")) {
                    if ($.inArray(input.value, filtervals) > -1) {
                      hide = false;
                      return;
                    }
                  }
                }
              });

              if (hide) {
                input.parentNode.parentNode.classList.add('hidden');
              } else {
                input.parentNode.parentNode.classList.remove('hidden');
              }
            }
          });
        }
      });
    }
  }]);

  return Filter;
}();

document.querySelectorAll('.filter').forEach(function (ele) {
  var filter = new Filter(ele);
});

var FormCarousel =
/*#__PURE__*/
function (_Carousel) {
  _inherits(FormCarousel, _Carousel);

  function FormCarousel(root) {
    var _this27;

    _classCallCheck(this, FormCarousel);

    _this27 = _possibleConstructorReturn(this, _getPrototypeOf(FormCarousel).call(this, root));

    _this27.root.setAttribute('novalidate', true);

    _this27.statusBox = _this27.root.querySelector('.status');
    _this27.progress = _this27.root.querySelector('.form-progress div');
    _this27.status = {
      message: '',
      type: null
    };
    return _this27;
  }

  _createClass(FormCarousel, [{
    key: "next",
    value: function next() {
      var requireds = this.items[this.index].querySelectorAll('[required]');
      var error = false;

      for (var i = 0; i < requireds.length; i++) {
        if (!requireds[i].checkValidity()) {
          error = true;
          this.status.message = requireds[i].validationMessage;
          this.status.type = 'error';
          requireds[i].focus();
          break;
        }
      }

      if (!error) {
        this.status.message = '';
        this.status.type = null;
        this.index++;
        var maxIndex = this.items.length - this.visibleAtOnce;

        if (this.index > maxIndex) {
          this.index = maxIndex;
        }

        this.update();
      }

      this.statusBox.classList.remove('error');
      this.statusBox.classList.remove('success');

      if (this.status.type) {
        this.statusBox.classList.add('visible');
        this.statusBox.classList.add(this.status.type);
      } else {
        this.statusBox.classList.remove('visible');
      }

      this.statusBox.innerHTML = this.status.type ? this.status.message : '';
    }
  }, {
    key: "initControl",
    value: function initControl() {
      var _this28 = this;

      this.container.addEventListener('transitionend', function (e) {
        if (e.target === _this28.container) {
          _this28.items.forEach(function (el, i) {
            if (i !== _this28.index) {
              el.style.visibility = 'hidden';
            }
          });
        }
      });
      this.container.querySelectorAll('input[type="checkbox"]').forEach(function (el) {
        el.addEventListener('change', function (e) {
          var group = _this28.root.querySelectorAll("input[name=\"".concat(el.name, "\"]"));

          var required = _this28.root.querySelectorAll("input[name=\"".concat(el.name, "\"]:required"));

          var checked = _this28.root.querySelectorAll("input[name=\"".concat(el.name, "\"]:checked"));

          if (required.length > 0) {
            if (checked.length > 0) {
              group.forEach(function (checkbox) {
                if (checkbox.checked) {
                  checkbox.required = true;
                } else {
                  checkbox.required = '';
                }
              });
            } else {
              group.forEach(function (checkbox) {
                checkbox.required = true;
              });
            }
          }
        });
      });
      this.root.addEventListener('submit', function (e) {
        var maxIndex = _this28.items.length - _this28.visibleAtOnce;

        if (_this28.index !== maxIndex) {
          e.preventDefault();

          _this28.next();
        }
      });
      this.root.querySelectorAll('input[type="radio"], select').forEach(function (el) {
        el.addEventListener('change', function (e) {
          if (el.type === 'radio') {
            _this28.items[_this28.index].querySelectorAll('.optional').forEach(function (el) {
              el.classList.remove('visible');
            });
          }

          if (el.dataset.for) {
            _this28.root.querySelector('[name="' + el.dataset.for + '"]').classList.add('visible');
          } else {
            window.setTimeout(function () {
              _this28.next();
            }, 300);
          }
        });
      });

      _get(_getPrototypeOf(FormCarousel.prototype), "initControl", this).call(this);
    }
  }, {
    key: "update",
    value: function update() {
      var _this29 = this;

      var animate = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      this.items.forEach(function (el, i) {
        if (i === _this29.index) {
          el.style.visibility = '';
        } else {
          if (animate === false) {
            el.style.visibility = 'hidden';
          }
        }
      });

      if (this.progress) {
        var progress = this.index / (this.items.length - 1);
        this.progress.style.transform = "scaleX(".concat(progress, ")");
      }

      _get(_getPrototypeOf(FormCarousel.prototype), "update", this).call(this, animate);
    }
  }]);

  return FormCarousel;
}(Carousel);

document.querySelectorAll('.form-carousel').forEach(function (element) {
  var carousel = new FormCarousel(element);
});
var drawer = document.querySelector('.nav-side-drawer');

if (drawer) {
  var shortcuts = document.querySelectorAll('a[data-shortcut]');
  var drawerToggle = drawer.querySelector('.toggle');
  shortcuts.forEach(function (v) {
    v.addEventListener('click', function (e) {
      e.stopPropagation();
      drawer.classList.toggle('expanded');
      if (mainNav) mainNav.goTo(v.dataset.shortcut, true);
    });
  });
  drawerToggle.addEventListener('click', function () {
    drawer.classList.toggle('expanded');

    if (drawer.classList.contains('expanded')) {
      if (mainNav) mainNav.goTo(false, true);
    }
  });
  window.addEventListener('click', function (e) {
    if (drawer.classList.contains('expanded') && !drawer.contains(e.target)) {
      drawer.classList.remove('expanded');
    }
  });
}